/****************************************************************************** 
* DISCLAIMER 

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. 

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws. 

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED. 

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software. 
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link: 
* http://www.renesas.com/disclaimer 
******************************************************************************/ 

/* Copyright (C) 2014. Renesas Electronics Corporation, All Rights Reserved. */

/****************************************************************************** 
* File Name : uart_interface.c
* Version : 1.00 
* Device(s) : RX651
* Tool-Chain : 
* OS : 
* H/W Platform : 
* Description : UART interface for RX651. 
* Operation : 
* Limitations : None 
****************************************************************************** 
* History : DD.MM.YYYY Version Description 
******************************************************************************/ 


/****************************************************************************** 
Includes 
******************************************************************************/ 
#include	"stdio.h"
#include	"uart_interface.h"
#ifdef R_HYBRID_PLC_RF
#include    "r_nwk_os_wrapper.h"
#endif

/******************************************************************************
Function Name:       getchar
Parameters:          none.
Return value:        Received character.
Description:         Get character from UART.
******************************************************************************/
#ifdef __IAR_SYSTEMS_ICC__
short
#else
short	_far 
#endif
_getchar(void)
{
   short		data = 0;

	data = RdrvUART_GetChar();

    return data;
}


/******************************************************************************
Function Name:       putchar
Parameters:          Character to be sent.
Return value:        none.
Description:         Put character to UART.
******************************************************************************/
#ifdef __IAR_SYSTEMS_ICC__
short
#else
short	_far
#endif
_putchar( short data )
{
#ifdef R_HYBRID_PLC_RF
	/* Add carriage return code */
	if(data == '\n')
	{
		RdrvUART_PutChar('\r');
	}
#endif
	RdrvUART_PutChar((unsigned char)data);
    return  1;
}


/******************************************************************************
Function Name:       gets
Parameters:          none.
Return value:        Received strings.
Description:         Get strings from UART.
******************************************************************************/
#ifdef __IAR_SYSTEMS_ICC__
char *
gets( char *buffer )
#else
char _far *
gets( char _far *buffer )
#endif
{
	short			data = 0;
	short			index = 0;
	unsigned char	Continueflag = 1;

	do{
#ifdef R_HYBRID_PLC_RF
		data = RdrvUART_GetChar();
#else
		data = getchar();
#endif

		switch(data){
			/* wait CR code */
			case	'\n':
			case	'\r':
				Continueflag = 0;
				break;

			/* End of strings */
			case	EOF:
#ifndef R_HYBRID_PLC_RF
				if(index == 0)
				{
					return ( char * )NULL;
				}
				Continueflag = 0;
#endif
				break;

	        /* Back space */
			case	'\b':
				if(index > 0)
				{
					/* Delete character */
					index--;
				}
				break;

			default:
				/* Get character */
				buffer[index] = (char)data;
				index++;
				break;
		}
#ifdef R_HYBRID_PLC_RF
		R_OS_DelayTaskMs(10);
#endif
	}while(Continueflag == 1);

	/* Add null code */
	buffer[index] = '\0';

	return buffer;
}


/******************************************************************************
Function Name:       RdrvGetcw
Parameters:          none.
Return value:        Received character. 
Description:         Get character from UART. 
******************************************************************************/
unsigned char RdrvGetcw(void)
{
	/* Wait untill character input */
	while(RdrvUART_GetLen() == 0);

	return (unsigned char)RdrvUART_GetChar();
}


/******************************************************************************
Function Name:       RdrvPrint
Parameters:          str
                       Pointer to string. 
Return value:        none.
Description:         Put strings to UART. 
******************************************************************************/
void RdrvPrint(char *str)
{
	/* Loop till end of strings */
	while (*str != '\0')
	{

#if defined(RDRV_UART_NEWLINE_CR_LF)
		if(*str == '\n')
		{
			RdrvUART_PutChar('\r');
		}
#endif

		/* Put character to UART */
		RdrvUART_PutChar((unsigned char)*str++);
	}
}


/******************************************************************************
Function Name:       RdrvPrintHex
Parameters:          Num
                       Number(HEX).
                     Len
                       Length of strings.
Return value:        none.
Description:         Put numerical strings to UART. 
******************************************************************************/
void RdrvPrintHex(unsigned long Num, unsigned char Len)
{
	unsigned char	tmp;

	/* Loop till end of strings */
	while(Len > 0)
	{
		/* Get digit */
		tmp = (unsigned char)((Num >> ((Len - 1) * 4)) & 0x0F);

		/* numerical -> HEX character and put to UART */
		if(tmp <= 0x09)
		{
			RdrvUART_PutChar('0' + tmp);
		}
		else
		{
			RdrvUART_PutChar('A' + (tmp - 0x0A));
		}

		/* Decrement length */
		Len--;
	}
}


/******************************************************************************
Function Name:       RdrvPrintDec
Parameters:          DecNum
                       Number(DEC).
                     Len
                       Length of strings.
Return value:        none.
Description:         Put numerical strings to UART. 
******************************************************************************/
void RdrvPrintDec(long DecNum, unsigned char Len, unsigned char SupCh)
{
	long			digit = 1;
	unsigned char	SupressFlg = 1;
	unsigned char	Num;
	unsigned char	i;

	/* minus number? */
	if(DecNum<0)
	{
		/* Put "-" character */
		RdrvUART_PutChar('-');
		DecNum = -DecNum;
	}

	/* Count digit */
	for(i=1; i<Len; i++)
	{
		digit *= 10;
	}

	while(digit > 0)
	{
		if(digit == 1)
		{
			/* Suppression off (Lowest digit) */
			SupressFlg = 0;
		}

		/* Get top digit number */
		Num = (unsigned char)((DecNum / digit) % 10);

		if(SupressFlg == 1)
		{
			if(Num == 0)
			{
				if(SupCh != '\0')
				{
					RdrvUART_PutChar(SupCh);
				}
			}
			else
			{
				/* Put a character(top digit number) */
				RdrvUART_PutChar((unsigned char)('0' + Num));

				/* Supression off */
				SupressFlg = 0;
			}
		}
		else
		{
			/* Put a character(top digit number) */
			RdrvUART_PutChar((unsigned char)('0' + Num));
		}

		/* remove top digit number and move a figure one place to the right */
		DecNum = DecNum - (Num * digit);
		digit = digit / 10;
	}
}


/******************************************************************************
Function Name:       RdrvInputHex
Parameters:          l
                       Pinter to number(HEX).
                     len
                       Length of strings.
Return value:        none.
Description:         Get numerical strings from UART. 
******************************************************************************/
unsigned long RdrvInputHex(unsigned long *Num, unsigned char Len)
{
	unsigned char	ch;
	unsigned char	index = 0;
	unsigned char	ContinueFlag = 1;

	while(ContinueFlag == 1)
	{
		/* Get character from UART */
		ch = getcw();

		switch(ch)
		{
			case	0x1B:	/* ESC */
				index = 0;
				ContinueFlag = 0;
				break;

			case	0x0A:	/* Line feed */
			case	0x0D:	/* Carriage return */
				ContinueFlag = 0;
				break;

			case	0x08:	/* BS */
			case	0x7F:	/* DEL */
				/* Delete character */
				if(index > 0)
				{
					(*Num) = (*Num) >> 4;
					index--;

					RdrvUART_PutChar(0x08);
					RdrvUART_PutChar(0x20);		/* Space */
					RdrvUART_PutChar(0x08);
				}
				break;

			default:
				if((ch >= 0x30) && (ch <= 0x39))			/* '0'-'9' */
				{
					*Num = (*Num) << 4;
					*Num |= (ch - 0x30) & 0x0F;
					index++;
				}else
				if((ch >= 0x41) && (ch <= 0x5A))			/* 'A'-'Z' */
				{
					*Num = (*Num) << 4;
					*Num |= ((ch - 0x41) & 0x0F) + 0x0A;
					index++;
				}else
				if((ch >= 0x61) && (ch <= 0x7A))			/* 'a'-'z' */
				{
					*Num = (*Num) << 4;
					*Num |= ((ch - 0x61) & 0x0F) + 0x0A;
					index++;
				}
				break;
		}

		if(index >= Len)
		{
			ContinueFlag = 0;
		}
	}

	/* add "CR" and "LF" */
	RdrvUART_PutChar(0x0D);
	RdrvUART_PutChar(0x0A);

	return index;
}


