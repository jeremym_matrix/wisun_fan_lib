/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      r_os_wrapper_def.h
   \version   1.00
   \brief     Macro prototypes for OS abstractions definition API
 */

#ifndef R_OS_WRAPPER_DEF_H
#define R_OS_WRAPPER_DEF_H

// Empty macros to allow including r_os_wrapper_id_config with the implementation provided in r_os_wrapper.c

#define R_OS_HEAP_LIST_BEGIN
#define R_OS_HEAP_LIST_ENTRY(id, size)
#define R_OS_HEAP_LIST_END


#define R_OS_MAILBOX_LIST_BEGIN
#define R_OS_MAILBOX_LIST_ENTRY(id)
#define R_OS_MAILBOX_LIST_END


#define R_OS_FLAG_LIST_BEGIN
#define R_OS_FLAG_LIST_ENTRY(id)
#define R_OS_FLAG_LIST_END


#define R_OS_SEMAPHORE_LIST_BEGIN
#define R_OS_SEMAPHORE_LIST_ENTRY(id, max, initial)
#define R_OS_SEMAPHORE_LIST_END


#define R_OS_TIMER_LIST_BEGIN
#define R_OS_TIMER_LIST_ENTRY(id, func, period_millis, oneshot)
#define R_OS_TIMER_LIST_END


#define R_OS_TASK_LIST_BEGIN
#define R_OS_TASK_LIST_ENTRY(id, name, func, stack, priority, suspended)
#define R_OS_TASK_LIST_END


#endif /* R_OS_WRAPPER_DEF_H */
