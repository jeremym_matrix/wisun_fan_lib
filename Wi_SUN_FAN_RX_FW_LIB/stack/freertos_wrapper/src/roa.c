/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
/*******************************************************************************
 * file name    : roa.c
 * description  : The OS Abstraction module for MAC sub layer.
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/* --- --- */
#ifdef RM_NON_STATIC
#define RM_STATIC
#else
#define RM_STATIC static
#endif

/* --- --- */
#include "r_stdint.h"
#include "mac_api.h"
#include "roa.h"
#include "roa_config.h"
#include "roa_intr.h"

/* --- --- */
// R_LOG_PREFIX is processed by RLog code generator
#define R_LOG_PREFIX RTOS
#include "r_log_internal.h"
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
#include "r_loggen_roa.h"
#endif

/* --- --- */
typedef struct
{
    RoaQEntry* freeQ;
    RoaMsgQ*   pEventMsgQ;
} RoaQEntryFreeT;

RM_STATIC RoaQEntryFreeT RoaQEntryFree;

/***********************************************************************
 * program start
 **********************************************************************/

/***********************************************************************
 *  function name  : roaInitQueue
 *  -------------------------------------------------------------------
 *  parameters     : RoaResourcesT *pResources
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
RM_STATIC void roaInitQueue(RoaResourcesT* pRoaResources)
{
    int16_t i;

    RoaQEntryFree.freeQ = pRoaResources->pQBlk;

    for (i = 0; i < (pRoaResources->numQBlk - 1); i++)
    {
        pRoaResources->pQBlk[i].next = (void*)&pRoaResources->pQBlk[i + 1];
        pRoaResources->pQBlk[i].data = NULL;
    }
    pRoaResources->pQBlk[i].next = NULL;
    pRoaResources->pQBlk[i].data = NULL;
}

/***********************************************************************
 *  function name  : RoaInitMsgQ
 *  -------------------------------------------------------------------
 *  parameters     : RoaMsgQ *queue
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
void RoaInitMsgQ(RoaMsgQ* queue)
{
    if (queue == NULL)
    {
        return;
    }
    queue->first = NULL;
    queue->last = NULL;
}

/***********************************************************************
 *  function name  : roaq_get_free_blk
 *  -------------------------------------------------------------------
 *  parameters     : none
 *  -------------------------------------------------------------------
 *  return value   : RoaQEntry *free_blk
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
RM_STATIC RoaQEntry* roaq_get_free_blk(void)
{
    RoaQEntry* free_blk;

    roa_psw_t bkup_psw;

    ROA_ALL_DI(bkup_psw);

    free_blk = (RoaQEntry*)RoaQEntryFree.freeQ;
    if (free_blk != NULL)
    {
        RoaQEntryFree.freeQ = free_blk->next;
    }

    ROA_ALL_EI(bkup_psw);

    return free_blk;
}

/***********************************************************************
 *  function name  : roaq_rel_free_blk
 *  -------------------------------------------------------------------
 *  parameters     : RoaQEntry *free_blk
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
RM_STATIC void roaq_rel_free_blk(RoaQEntry* free_blk)
{
    roa_psw_t bkup_psw;

    if (free_blk == NULL)
    {
        return;
    }

    ROA_ALL_DI(bkup_psw);

    /* Add at the beginning of the free queue */
    free_blk->data = NULL;
    free_blk->next = RoaQEntryFree.freeQ;

    RoaQEntryFree.freeQ = free_blk;

    ROA_ALL_EI(bkup_psw);

}

/***********************************************************************
 *  function name  : RoaPutQ
 *  -------------------------------------------------------------------
 *  parameters     : void *pMsg
 *                 : RoaMsgQ *queue
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
void RoaPutQ(void* pMsg, RoaMsgQ* queue)
{
    RoaQEntry* pEntry;
    roa_psw_t bkup_psw;

    if (queue == NULL || pMsg == NULL)
    {
        return;
    }

    pEntry = roaq_get_free_blk();
    if (pEntry == NULL)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_189();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return;
    }

    /* Add the message at the end of the queue */
    pEntry->data = (void*)pMsg;
    pEntry->next = NULL;

    ROA_ALL_DI(bkup_psw);

    if (queue->last != NULL)
    {
        ((RoaQEntry*)(queue->last))->next = (void*)pEntry;
    }

    queue->last = (void*)pEntry;

    if (queue->first == NULL)
    {
        queue->first = queue->last;
    }

    ROA_ALL_EI(bkup_psw);

}

/***********************************************************************
 *  function name  : RoaPutBackQ
 *  -------------------------------------------------------------------
 *  parameters     : void *pMsg
 *                 : RoaMsgQ *queue
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
void RoaPutBackQ(void* pMsg, RoaMsgQ* queue)
{
    RoaQEntry* pEntry;
    roa_psw_t bkup_psw;

    if (queue == NULL || pMsg == NULL)
    {
        return;
    }

    pEntry = roaq_get_free_blk();
    if (pEntry == NULL)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_238();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return;
    }

    pEntry->data = (void*)pMsg;

    ROA_ALL_DI(bkup_psw);

    /* Add the message at the end of the queue */
    pEntry->next = queue->first;
    queue->first = (void*)pEntry;

    if (queue->last == NULL)
    {
        queue->last = queue->first;
    }

    ROA_ALL_EI(bkup_psw);

}

/***********************************************************************
 *  function name  : RoaGetQ
 *  -------------------------------------------------------------------
 *  parameters     : RoaMsgQ *queue
 *  -------------------------------------------------------------------
 *  return value   : void *pMsg
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
void* RoaGetQ(RoaMsgQ* queue)
{
    /* Function to pick up the queue */
    void* pMsg;
    RoaQEntry* pEntry;
    roa_psw_t bkup_psw;

    if (queue == NULL || queue->first == NULL)
    {
        return NULL;
    }

    ROA_ALL_DI(bkup_psw);

    pEntry = (RoaQEntry*)queue->first;
    pMsg = (RoaMsgT*)pEntry->data;

    /* De-queue the message */
    queue->first = (void*)(pEntry->next);

    /* When the queue become empty */
    if (queue->first == NULL)
    {
        queue->last = NULL;
    }

    ROA_ALL_EI(bkup_psw);

    roaq_rel_free_blk(pEntry);

    return pMsg;
}

/***********************************************************************
 *  function name  : RoaInit
 *  -------------------------------------------------------------------
 *  parameters     : RoaResourcesT *pResources
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
void RoaInit(RoaResourcesT* pResources)
{
#if defined(__RX)
    uint8_t i;
#else
    int8_t i;
#endif

    roaInitQueue(pResources);

    for (i = 0; i < pResources->numEvent; i++)
    {
        RoaInitMsgQ(&pResources->pEventMsgQ[i]);
    }

    RoaQEntryFree.pEventMsgQ = pResources->pEventMsgQ;
}

/***********************************************************************
 *  function name  : RoaSetFlg
 *  -------------------------------------------------------------------
 *  parameters     : uint8_t eventid
 *                 : uint16_t flgptn
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
void RoaSetFlg(uint8_t eventid, uint16_t flgptn)
{
    // This distinction is from the uItron version although also RoaISetFlg exists
    if (RoaIsInInterrupt())
    {
        R_OS_SetFlagFromISR(eventid, flgptn);
    }
    else
    {
        R_OS_SetFlag(eventid, flgptn);
    }
}

/***********************************************************************
 *  function name  : RoaISetFlg
 *  -------------------------------------------------------------------
 *  parameters     : uint8_t eventid
 *                 : uint16_t flgptn
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
void RoaISetFlg(uint8_t eventid, uint16_t flgptn)
{
    R_OS_SetFlagFromISR(eventid, flgptn);
}

/***********************************************************************
 *  function name  : RoaWaiFlg
 *  -------------------------------------------------------------------
 *  parameters     : uint16_t *pFlgptn
 *                 : uint8_t eventid
 *                 : uint16_t flgptn
 *                 : uint16_t tmo
 *  -------------------------------------------------------------------
 *  return value   : ROA_SUCCESS,ROA_ERR_TIMEOUT,ROA_ERR_FATAL
 *  -------------------------------------------------------------------
 *  description    : This function should be called from the task.
 **********************************************************************/
uint8_t RoaWaiFlg(uint16_t* pFlgptn, uint8_t eventid, uint16_t flgptn, uint16_t tmo)
{
    // uItron: "wait for any" and cleaning flags is part of configuration
    // PRQA S 2740 2
    while (1)
    {
        r_os_result_t res = R_OS_WaitForFlag(eventid, flgptn, 1, 0, tmo, pFlgptn);
        if (tmo == ROA_TMO_FEVR && res == R_OS_RESULT_TIMEOUT)
        {
            continue;
        }
        switch (res)
        {
            case R_OS_RESULT_SUCCESS:
                return ROA_SUCCESS;

            case R_OS_RESULT_TIMEOUT:
                return ROA_ERR_TIMEOUT;

            default:
                return ROA_ERR_FATAL;
        }
    }
}

/***********************************************************************
 *  function name  : RoaGetMsg
 *  -------------------------------------------------------------------
 *  parameters     : uint8_t eventid
 *  -------------------------------------------------------------------
 *  return value   : void *p_msg
 *  -------------------------------------------------------------------
 *  description    : This function should be called from the task.
 **********************************************************************/
void* RoaGetMsg(uint8_t eventid)
{
    return RoaGetQ(RoaQEntryFree.pEventMsgQ);
}

/***********************************************************************
 *  function name  : RoaGetSem
 *  -------------------------------------------------------------------
 *  parameters     : uint8_t semid
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
void RoaGetSem(uint8_t semid)
{
    R_OS_WaitSemaphore(semid);
}

/***********************************************************************
 *  function name  : RoaReleaseSem
 *  -------------------------------------------------------------------
 *  parameters     : uint8_t semid
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    :
 **********************************************************************/
void RoaReleaseSem(uint8_t semid)
{
    R_OS_PostSemaphore(semid);
}

/******************************************************************************
 *****************************************************************************/

/***********************************************************************
 *  function name  : RoaAllocBlf
 *  -------------------------------------------------------------------
 *  parameters     : uint8_t mplid
 *  -------------------------------------------------------------------
 *  return value   : void *pMsg
 *  -------------------------------------------------------------------
 *  description    : This function should be called from the task.
 **********************************************************************/
void* RoaAllocBlf(uint8_t mplid, size_t size)
{
    return R_OS_MsgAlloc(mplid, size);
}

/***********************************************************************
 *  function name  : RoaRelBlf
 *  -------------------------------------------------------------------
 *  parameters     : uint8_t mpfid
 *                 : void *pMsg
 *  -------------------------------------------------------------------
 *  return value   : ROA_SUCCESS, ROA_ERR_FATAL
 *  -------------------------------------------------------------------
 *  description    : This function should be called from the task.
 **********************************************************************/
uint8_t RoaRelBlf(void* pMsg)
{
    R_OS_MsgFree(pMsg);
    return R_OS_RESULT_SUCCESS;
}

/***********************************************************************
 *  function name  : RoaGetTskId
 *  -------------------------------------------------------------------
 *  parameters     : none
 *  -------------------------------------------------------------------
 *  return value   : task Id
 *  -------------------------------------------------------------------
 *  description    : This function should be called from the task.
 **********************************************************************/
uint8_t RoaGetTskId(void)
{
    return R_OS_GetTaskId();
}

/***********************************************************************
 *  function name  : RoaSndMsg
 *  -------------------------------------------------------------------
 *  parameters     : void *pMsg
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    : This function should be called from the task.
 **********************************************************************/
void RoaSndMsg(void* pMsg)
{
    uint8_t eventid = ID_mac_flg;

    ((RoaMsgHdrT*)(pMsg))->osMsgHeader.task_id = RoaGetTskId();

    RoaPutQ(pMsg, RoaQEntryFree.pEventMsgQ);
    RoaSetFlg(eventid, ROA_FLG_MSG);
}

/***********************************************************************
 *  function name  : RoaSndMsgFromISR
 *  -------------------------------------------------------------------
 *  parameters     : void *pMsg
 *  -------------------------------------------------------------------
 *  return value   : none
 *  -------------------------------------------------------------------
 *  description    : This function should be called from ISRs.
 **********************************************************************/
void RoaSndMsgFromISR(void* pMsg)
{
    uint8_t eventid = ID_mac_flg;

    ((RoaMsgHdrT*)(pMsg))->osMsgHeader.task_id = RoaGetTskId();

    RoaPutQ(pMsg, RoaQEntryFree.pEventMsgQ);
    RoaISetFlg(eventid, ROA_FLG_MSG);
}

/*******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/
