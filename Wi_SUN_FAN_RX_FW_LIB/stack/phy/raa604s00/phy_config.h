/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_config.h
 * description	: This is the RF driver's configuration code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
#ifndef _PHY_CONFIG_H
#define _PHY_CONFIG_H

/***************************************************************************************************************
 * User definitions
 **************************************************************************************************************/
#define RP_USR_RF_DFLT_TRANSMIT_POWER			89							// default Transmit Power(gain set)
#define RP_USR_RF_DFLT_CCA_VTH					-93							// default CCA Vthreshold
#define RP_USR_RF_DFLT_PROFILE_SPECIFIC_MODE	RP_SPECIFIC_NORMAL			// default Profile Specific Mode
#define RP_USR_RF_DFLT_TX_ANTENNA_SWITCH_ENA	RP_FALSE					// default Tx Antenna Switch Enable
#define RP_USR_RF_DFLT_ANTENNA_DIVERSITY_ENA	RP_FALSE					// default Antenna Diversity Enable
#define RP_USR_RF_RSSI_LOSS						0							// RSSI Loss
#define RP_USR_RF_CLK_SOURCE_SELECT				RP_RFCLK_SOURCE_CRYSTAL		// clock Source for RF
#define RP_USR_RF_LIMIT_TMR_SELECT				RP_CLK_LIMIT_TMR_FASTOCO	// clock Source for tx limit control
#define RP_USR_RF_TAU_CH_SELECT					2							// Select TAU Channel 1:ch1, 2:ch2

#endif // _PHY_CONFIG_H
/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
