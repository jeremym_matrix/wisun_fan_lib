/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
/*******************************************************************************
 * file name	: phy_mac.c
 * description	: 
 *******************************************************************************
 * Copyright (C) 2018 Renesas Electronics Corporation.
 ******************************************************************************/
#include <machine.h>
#include "iodefine.h"
#include "hardware.h"

#ifdef RP_WISUN_FAN_STACK
#include "r_stdint.h"
#else
#include "r_types.h"
#endif

#include "phy.h"
#include "phy_def.h"
#include "phy_drv.h"


#ifdef R_USE_RENESAS_STACK
#include "int_vec_def.h"
#endif

#ifdef R_USE_RENESAS_STACK
/* Mac Callback Operation */
//	typedef void (*RmMacOSCallbackT)(void);
typedef void (*RmMacOSCallbackT)(uint8_t);
volatile RmMacOSCallbackT pRmMacOSCback;
volatile uint8_t RmWupTskEventId[2];
volatile uint8_t RmWupTskEventIdNum = 0;
/******************************************************************************
Function Name:       RpWupTsk
Parameters:          none
Return value:        none
Description:         set wakeup-task handler.
******************************************************************************/
void RpWupTsk(uint8_t eventId)
{
	if (RmWupTskEventIdNum < 2)
	{
		RmWupTskEventId[RmWupTskEventIdNum++] = eventId;
	}
#if	defined(__RX)
	TPU2.TGRA = 1;
	TPU2.TIER.BIT.TGIEA = 1;	/* interrupt enable */
	IEN(PERIB,INTB138) = 1;
    TPUA.TSTR.BIT.CST2 = 1;
#endif	/* defined(__RX) */
}
#endif /* R_USE_RENESAS_STACK */

#ifdef R_USE_RENESAS_STACK
#ifdef __IAR_SYSTEMS_ICC__
#pragma section="IntPRG"
#else
#pragma section IntPRG
#endif
/******************************************************************************
Function Name:       RpWupTskHdr
Parameters:          none
Return value:        none
Description:         This handler is OS depending interrupt for "iset_flg".
******************************************************************************/
void
RpWupTskHdr(void)
{
	uint8_t   j;
#if	defined(__RX)
#ifdef __IAR_SYSTEMS_ICC__
	asm("setpsw i");
#else
	setpsw_i();
#endif

	TPU2.TIER.BIT.TGIEA = 0;	/* interrupt disable */
    IEN(PERIB,INTB138) = 0;
    
	TPUA.TSTR.BIT.CST2 = 0;
#endif	/* defined(__RX) */
	for (j = 0; j < RmWupTskEventIdNum; j++)
	{
		(*pRmMacOSCback)(RmWupTskEventId[j]);
	}
	RmWupTskEventIdNum = 0;
}
#endif /* R_USE_RENESAS_STACK */

#ifdef R_USE_RENESAS_STACK
#if	defined(__RX)
/******************************************************************************
Function Name:       Disable Interrupt
Parameters:          none
Return value:        none
Description:         Disable interrupt.
******************************************************************************/
#pragma inline_asm _DI_Int_Prc
static void _DI_Int_Prc(void)
{
	mvfc	isp, r2
	add		#(8*4),r2
	mov.l [r2], r1
	and 	#0fffeffffh, r1
	mov.l r1, [r2]
}

/******************************************************************************
Function Name:       Disable RF IC Interrupt
Parameters:          none
Return value:        none
Description:         Disable RF IC interrupt.
******************************************************************************/
#pragma inline_asm _IPL_Int_Prc
static void _IPL_Int_Prc(void)
{
	mvfc	isp, r2
	add		#(8*4),r2
	mov.l [r2], r1
	and 	#0f0ffffffh, r1
	or 		#005000000h, r1		; IPL=5
	mov.l r1, [r2]
}
#pragma inline_asm _IPL_Int_0_Prc
static void _IPL_Int_0_Prc(void)
{
	mvfc	isp, r2
	add		#(8*4),r2
	mov.l [r2], r1
	and 	#0f0ffffffh, r1		; IPL=0
/* Start: Allow only interrupts on RP_PHY_INTLEVEL_SYSCALLTimer level.      */
/*        Mask TPU3 timer interrupt level (Frequency Hopping Timer),        */
/*        to solve reentrant issue                                          */
	or 		#004000000h, r1		; IPL=4
/* End:                                                                     */
    mov.l  r1, [r2]
}

/******************************************************************************
Function Name:       Enable RF IC Interrupt
Parameters:          none
Return value:        none
Description:         Enable RF IC interrupt.
******************************************************************************/
#pragma inline_asm _UM_Int_Prc
static void _UM_Int_Prc(void)
{
	mvfc	isp, r2
	mov.l [r2], r1

	mvfc	isp, r2
	add		#(8*4),r2
	mov.l r1, [r2]
}

void
_DI_Int(void)
{
	_DI_Int_Prc();
}

void
_IPL_Int(void)
{
	_IPL_Int_Prc();
}

void
_IPL_0_Int(void)
{
	_IPL_Int_0_Prc();
}


void
_UM_Int(void)
{
	_UM_Int_Prc();
}
#endif /* defined(__RX) */
#endif /* R_USE_RENESAS_STACK */

/*******************************************************************************
 * Copyright (C) 2018 Renesas Electronics Corporation.
 ******************************************************************************/

