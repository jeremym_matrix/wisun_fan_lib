/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* (C) 2014-2015 Renesas Electronics Corporation All rights reserved.
*******************************************************************************/

/******************************************************************************
* File Name     : r_log_flags.h
* Description   : Compile time flags of the logging module
******************************************************************************/
/******************************************************************************
* History       : DD.MM.YYYY Version Description
*               : 30.01.2015 1.00    First Release
*******************************************************************************/


#ifndef R_LOG_FLAGS_H
#define R_LOG_FLAGS_H

#define R_LOG_SEVERITY_OFF  0  //!< This logging severity disables logging
#define R_LOG_SEVERITY_ERR  1  //!< The logging severity indicating errors
#define R_LOG_SEVERITY_WARN 2  //!< The logging severity indicating warnings
#define R_LOG_SEVERITY_DBG  3  //!< The logging severity indicating debug messages

#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF

#define R_LOG_TIMESTAMP_OFF      0  //!< Disable timestamps in log records
#define R_LOG_TIMESTAMP_ABSOLUTE 1  //!< Use an absolute timestamp in each log record
#define R_LOG_TIMESTAMP_RELATIVE 2  //!< Use relative timestamps encoded as varints in log records

#endif /* R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF */

#endif /* R_LOG_FLAGS_H */
