/**
 * \addtogroup uip6
 * @{
 */

/**
 * \file
 *    Header file for IPv6-related data structures
 * \author Mathilde Durvy <mdurvy@cisco.com>
 * \author Julien Abeille <jabeille@cisco.com>
 *
 */
/*
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#ifndef UIP_DS6_H_
#define UIP_DS6_H_

#include "net/uip.h"
#include "net/uip-ds6-nbr.h"
#include "net/uip.h"
#include "lib/list.h"

/*--------------------------------------------------*/
/* Unicast address list*/
#define UIP_DS6_ADDR_NB R_ND_IPV6_ADDRESS_LIST_LENGTH


/*--------------------------------------------------*/

/** \brief Possible states for the an address  (RFC 4862) */
#define ADDR_TENTATIVE  0
#define ADDR_PREFERRED  1
#define ADDR_DEPRECATED 2

/** \brief How the address was acquired: Autoconf, DHCP or manually */
#define  ADDR_ANYTYPE   0
#define  ADDR_AUTOCONF  1
#define  ADDR_DHCP      2
#define  ADDR_MANUAL    3

/** \brief General DS6 definitions */
#define FOUND           0
#define FREESPACE       1
#define NOSPACE         2

/*--------------------------------------------------*/

/** * \brief Unicast address structure */
typedef enum
{
    R_UIP_IPV6_ADDR_FREE = 0x00, //!< Address entry is free
    R_UIP_IPV6_ADDR_CHECK,       //!< Address needs to be checked
    R_UIP_IPV6_ADDR_VALID,       //!< Address is valid

} r_uip_ipv6addr_status_t;

#ifdef __ICCRL78__
R_HEADER_UTILS_PRAGMA_PACK_1
#endif
typedef struct uip_ds6_addr
{
    uip_ipaddr_t            ipaddr;     //!< IPv6 address
    uint8_t                 state;      //!< State of the IPv6 address
    uint8_t                 type;       //!< Type of address
    uint8_t                 isInfinite; //!< Infinite valid time flag
    uint8_t                 isUsed;     //!< Flag to identify if the address is used
    r_uip_ipv6addr_status_t status;     //!< IPv6 address status
} uip_ds6_addr_t;
#ifdef __ICCRL78__
R_HEADER_UTILS_PRAGMA_PACK_DEFAULT
#endif

/** \brief  Interface structure (contains all the interface variables) */
typedef struct uip_ds6_netif
{
    uip_ds6_addr_t* addr_list;
} uip_ds6_netif_t;

/** \brief Generic type for a DS6, to use a common loop though all DS */
typedef struct uip_ds6_element
{
    uip_ipaddr_t            ipaddr;     //!< IPv6 address
    uint8_t                 state;      //!< State of the IPv6 address
    uint8_t                 type;       //!< Type of address
    uint8_t                 isInfinite; //!< Infinite valid time flag
    uint8_t                 isUsed;     //!< Flag to identify if the address is used
    r_uip_ipv6addr_status_t status;     //!< IPv6 address status
} uip_ds6_element_t;

/*---------------------------------------------------------------------------*/
extern uip_ds6_netif_t uip_ds6_if;

/*---------------------------------------------------------------------------*/
/** \brief Initialize data structures */
void uip_ds6_init(const uint8_t* eui64);

/** \brief Generic loop routine on an abstract data structure, which generalizes
 * all data structures used in DS6 */
uint8_t uip_ds6_list_loop(uip_ds6_element_t* list, uint8_t size,
                          uint16_t elementsize, const uip_ipaddr_t* ipaddr,
                          uint8_t ipaddrlen,
                          uip_ds6_element_t** out_element);

/** @} */


/** \name Unicast address list basic routines */
/** @{ */
/** \brief Add a unicast address to the interface */
uip_ds6_addr_t* uip_ds6_addr_add(const uip_ipaddr_t* ipaddr, unsigned long vlifetime, uint8_t type);
void            uip_ds6_addr_rm(uip_ds6_addr_t* addr);
uip_ds6_addr_t* uip_ds6_addr_lookup(const uip_ipaddr_t* ipaddr);
uip_ds6_addr_t* uip_ds6_addr_lookup_by_prefix(const uip_ipaddr_t* ipaddr, uint8_t prefixLength);
uip_ds6_addr_t* uip_ds6_get_link_local(int8_t state);
uip_ds6_addr_t* uip_ds6_get_global(int8_t state);

/** @} */

/** \brief set the last 64 bits of an IP address based on the MAC address */
void uip_ds6_set_addr_iid(uip_ipaddr_t* ipaddr, uip_lladdr_t* lladdr);

/** \brief Get the number of matching bits of two addresses */
uint8_t uip_ds6_get_match_length(uip_ipaddr_t* src, uip_ipaddr_t* dst);

/** \brief Source address selection, see RFC 3484 */
void uip_ds6_select_src(uip_ipaddr_t* src, uip_ipaddr_t* dst);


/** \name Macros to check if an IP address is mine */
/** @{ */
#define uip_ds6_is_my_addr(addr) (uip_ds6_addr_lookup(addr) != NULL)

/** @} */
/** @} */

#endif /* UIP_DS6_H_ */
