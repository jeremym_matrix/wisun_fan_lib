/*******************************************************************************
 * File Name    : r_nwk.h
 * Version      : 1.00
 * Description  : Header for the module that implements the network task processing
 ******************************************************************************/
#ifndef R_NWK_H
#define R_NWK_H


#include <stdlib.h>
#include <string.h>

#include "r_nwk_api.h"
#include "r_nwk_inter_config.h"

#include "mac_mlme.h"
#include "mac_mcps.h"
#include "r_mem_tools.h"
#include "r_io_vec.h"
#include "r_byte_swap.h"
#include "r_checksum.h"
#include "r_os_wrapper.h"
#include "r_nd_context_handling.h"
#include "r_6lowpan.h"
#include "r_6lowpan_headers.h"
#include "r_ipv6_headers.h"
#include "r_ipv6_helper.h"
#include "r_hc_6282.h"
#include "r_hc_6282_headers.h"
#include "r_hc_6282_comp_helper.h"
#include "r_hc_6282_comp_next_hdr.h"
#include "r_hc_6282_uncomp_address.h"
#include "r_hc_6282_uncomp_next_hdr.h"

#include "r_icmpv6.h"
#include "r_icmpv6_nd.h"
#include "r_icmpv6_process.h"
#include "r_udpv6.h"
#include "r_rpl_api.h"
#include "r_nd.h"
#include "r_nd_context_handling.h"

#include "r_frag.h"
#include "r_frag_tx.h"
#include "r_frag_rx.h"

#include "r_mac_init.h"
#include "r_mac_wrapper.h"
#include "r_6lp_tx_processing.h"
#include "r_6lp_rx_processing.h"

#if !R_DEV_DISABLE_AUTH
#include "r_auth_types.h"
#endif

#include "r_rpl_api.h"
#include "r_nwk_task.h"
#include "r_nwk_global.h"

#if   defined(__RX)
#elif defined(__RL78__)
#include "hardware.h"
#if defined(__ICCRL78__)
#include "RL78G1H.h"
#else
#include "MB_RLRAA604S00.h"
#endif
#include "uart_interface.h"
#endif
#include "stdio.h"

#endif  /* R_NWK_H */
