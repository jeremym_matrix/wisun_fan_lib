/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2012 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_6lp_tx_processing.h
 * Version     : 1.0
 * Description : Header file for the 6lowpan tx processing
 ******************************************************************************/

/*!
   \file      r_6lp_tx_processing.h
   \version   1.0
   \brief     Header file for the 6lowpan tx processing
 */

#ifndef R_6LP_TX_PROCESSING_H
#define R_6LP_TX_PROCESSING_H

/*!
 * Perform the 6lowpan tx processing for the specified IPv6 packet.
 * @param packet Pointer to the IPv6 packet that should be sent.
 * @param txOptions Tx options (enable/disable security).
 * @retval R_RESULT_SUCCESS if the packet was successfully sent.
 * @retval R_RESULT_INVALID_PARAMETER if the IPv6 destination address is equal to the IPv6 address of this device.
 * @retval R_RESULT_FAILED if any of the functions called by this function failed.
 */
r_result_t R_6LP_TxProcessing(const uint8_t* packet, uint16_t txOptions, void* vp_nwkGlobal);

#endif /* R_6LP_TX_PROCESSING_H */
