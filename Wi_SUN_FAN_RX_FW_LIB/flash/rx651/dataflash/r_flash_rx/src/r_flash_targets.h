/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No 
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, 
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM 
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES 
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS 
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
* this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer 
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/
/***********************************************************************************************************************
* File Name    : r_flash_targets.h
* Description  : This is a private header file used internally by the FLASH module. It should not be modified or
*                included by the user in their application.
***********************************************************************************************************************/
/***********************************************************************************************************************
* History : DD.MM.YYYY Version Description
*           02.08.2017 1.00    First Release
***********************************************************************************************************************/

#ifndef FLASH_TARGETS_HEADER_FILE
#define FLASH_TARGETS_HEADER_FILE

#include <stdbool.h>
#include <stddef.h>
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
/* Target Device: RX651E, R5F5651EDDFP */
#define MCU_CFG_PART_MEMORY_SIZE    0xE			/* 2 MByte Code FLASH */
#define MCU_ROM_SIZE_BYTES          2097152		/* 2 MByte Code FLASH */
#define MCU_RAM_SIZE_BYTES          262144		/* 256 kByte RAM (excluding expansion RAM) */
#define MCU_DATA_FLASH_SIZE_BYTES   32768		/* 32 kByte Data FLASH */

#define MCU_RX651
#define MCU_RX65N
#define MCU_RX65_ALL

#define MCU_CFG_ICLK_HZ            (80000000)	/* internal clock */
#define MCU_CFG_FCLK_HZ            (16000000)	/* external frequency */

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/


/***********************************************************************************************************************
Exported global variables
***********************************************************************************************************************/


/***********************************************************************************************************************
Exported global functions (to be accessed by other files)
***********************************************************************************************************************/

#endif /* FLASH_TARGETS_HEADER_FILE */

/* end of file */
