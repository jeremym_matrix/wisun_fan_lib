#ifndef R_AUTH_CONFIG_H
#define R_AUTH_CONFIG_H

#if R_BORDER_ROUTER_ENABLED
#include "r_nwk_inter_config.h"
#endif

#if R_DEV_DISABLE_AUTH
#undef R_AUTH_MEM_CAPACITY
#define R_AUTH_MEM_CAPACITY 8
#endif

// Unless explicitly set otherwise, R_BORDER_ROUTER_ENABLED implies R_BR_AUTHENTICATOR_ENABLED
#if !defined(R_BR_AUTHENTICATOR_ENABLED) && R_BORDER_ROUTER_ENABLED
#define R_BR_AUTHENTICATOR_ENABLED 1
#endif


// Heap size for supplicant-only configuration, see BR configuration below
#if !defined(R_AUTH_MEM_CAPACITY) && !R_BR_AUTHENTICATOR_ENABLED
// 64 bit systems requires ~35% more memory, but we double it since it's not used in production anyway (only for sim)
#define R_AUTH_MEM_CAPACITY (30 * 1024 * (sizeof(void*) > 4 ? 2 : 1))
#endif


#ifndef R_AUTH_EAP_FRAGMENT_SIZE
#define R_AUTH_EAP_FRAGMENT_SIZE 1398
#endif


#ifndef R_AUTH_JOIN_TIMER_BASE
#if R_DEV_FAST_JOIN
#define R_AUTH_JOIN_TIMER_BASE (60)
#else
// Spec: initial retransmission time of 5 minutes
#define R_AUTH_JOIN_TIMER_BASE (5 * 60)
#endif
#endif

#ifndef R_AUTH_JOIN_MAX_BACKOFF_DEFAULT
#if R_DEV_FAST_JOIN
#define R_AUTH_JOIN_MAX_BACKOFF_DEFAULT (5 * 60)
#else
// Spec: maximum retransmission time of 60 minutes
#define R_AUTH_JOIN_MAX_BACKOFF_DEFAULT (60 * 60)
#endif
#endif

#ifndef R_AUTH_JOIN_TIMER_BACKOFF_FACTOR
#if R_DEV_FAST_JOIN
#define R_AUTH_JOIN_TIMER_BACKOFF_FACTOR (1)
#else
// Spec: double interval
#define R_AUTH_JOIN_TIMER_BACKOFF_FACTOR (2)
#endif
#endif

#if R_BR_AUTHENTICATOR_ENABLED

#ifndef R_AUTH_BR_SUPPLICANTS_SIZE
//! The number of supplicants for which information is cached on the BR (LRU cache)
#define R_AUTH_BR_SUPPLICANTS_SIZE R_BR_PAN_SIZE
#endif

#ifndef R_AUTH_EAP_TLS_RETRY_THRESHOLD
//! The maximum time (in seconds) for which a BR tries to catch up EAP-TLS handshakes that failed due to insufficient resources.
//! This values applies to SUPs directly communicating with the LBR. */
#define R_AUTH_EAP_TLS_RETRY_THRESHOLD 180
#endif

#ifndef R_AUTH_EAP_TLS_RETRY_THRESHOLD_RELAY
//! The maximum time (in seconds) for which a BR tries to catch up EAP-TLS handshakes that failed due to insufficient resources.
//! This values applies to SUPs communicating with the LBR via a relay. Note: This value is chosen lower than R_AUTH_EAP_TLS_RETRY_THRESHOLD
//! to make sure timing information in the relay does not expire before resuming the delayed EAP-TLS handshake. */
#define R_AUTH_EAP_TLS_RETRY_THRESHOLD_RELAY 90
#endif

#ifndef R_AUTH_EAP_TLS_PTK_PMK_GAP_SIZE
/*! The authenticator will only resume postponed EAP-TLS handshakes if the gap between the number of PMKs and PTKs is less or
 *  equal to R_AUTH_EAP_TLS_PTK_PMK_GAP_SIZE. */
#define R_AUTH_EAP_TLS_PTK_PMK_GAP_SIZE 8
#endif

#ifndef R_EAP_TLS_SRV_SUP_TIMEOUT
/*!
 * The maximum time (in seconds) between two EAP-TLS messages from a supplicant before the BR considers the handshake
 * as failed. Default value is 100 seconds since the processing time on RL78/G1H is ~91 seconds for a chain of
 * 4 certificates, which is considered as worst case. This value might be reduced if the processing times are known to
 * be shorter for every device in the network.
 */
#define R_EAP_TLS_SRV_SUP_TIMEOUT 100
#endif

#ifndef R_EAP_TLS_SRV_MAX_CONCURRENT_SUPPLICANTS
//! The maximum number of supplicants that may perform EAP-TLS handshakes with this BR concurrently */
#define R_EAP_TLS_SRV_MAX_CONCURRENT_SUPPLICANTS 4
#endif

#ifndef R_EAP_TLS_SRV_HEAP_PER_SUPPLICANT
//! The number of bytes required on the border router to perform an EAP-TLS handshake */
#define R_EAP_TLS_SRV_HEAP_PER_SUPPLICANT 14840
#endif

#ifndef R_AUTH_MEM_CAPACITY
//! The memory capacity for the authentication heap */
#define R_AUTH_MEM_CAPACITY (((15 * 1024) + R_EAP_TLS_SRV_MAX_CONCURRENT_SUPPLICANTS * R_EAP_TLS_SRV_HEAP_PER_SUPPLICANT) * (sizeof(void*) > 4 ? 2 : 1))
// 64 bit systems requires ~35% more memory, but we double it since it's not used in production anyway (only for sim)
#endif

#ifndef R_AUTH_BR_RSN_SUPPLICANT_TIMEOUT
//! The time for which the RSN context of the BR is reserved for the current supplicant
#define R_AUTH_BR_RSN_SUPPLICANT_TIMEOUT 30
#endif

#ifndef R_AUTH_BR_GTK_EXPIRE_OFFSET_MINUTES_DEFAULT
//! The expiration time of a GTK is calculated as the expiration time of the GTK most recently installed at the Border Router plus GTK_EXPIRE_OFFSET
#define R_AUTH_BR_GTK_EXPIRE_OFFSET_MINUTES_DEFAULT (30 * 24 * 60)  // [FANWG-6.3.1.1]: 30 days
#endif

#ifndef R_AUTH_BR_GTK_NEW_ACTIVATION_TIME_DEFAULT
//! The time at which the Border Router activates the next GTK prior to expiration of the currently activated GTK.
#define R_AUTH_BR_GTK_NEW_ACTIVATION_TIME_DEFAULT (720)  // [FANWG-6.3.1.1]: resulting default value of 60 minutes
#endif

#ifndef R_AUTH_BR_REVOCATION_LIFETIME_REDUCTION_DEFAULT
//! Factor by which the active GTK lifetime is reduced during node revocation procedures
#define R_AUTH_BR_REVOCATION_LIFETIME_REDUCTION_DEFAULT (30)  // [FANWG-6.3.1.1]
#endif

#ifndef R_AUTH_BR_PMK_LIFETIME_MINUTES_DEFAULT
//! Default lifetime of a PMK
#define R_AUTH_BR_PMK_LIFETIME_MINUTES_DEFAULT (4 * 30 * 24 * 60)  // [FANWG-6.5.2.2]: 4 months
#endif

#ifndef R_AUTH_BR_PTK_LIFETIME_MINUTES_DEFAULT
//! Default lifetime of a PTK
#define R_AUTH_BR_PTK_LIFETIME_MINUTES_DEFAULT (2 * 30 * 24 * 60)  // [FANWG-6.5.2.2]: 2 months
#endif

#ifndef R_AUTH_BR_GTK_NEW_INSTALL_REQUIRED_DEFAULT
//! Percent of a GTKs lifetime after which a new GTK must be installed
#if !R_DEV_TBU_ENABLED
#define R_AUTH_BR_GTK_NEW_INSTALL_REQUIRED_DEFAULT 80
#else
#define R_AUTH_BR_GTK_NEW_INSTALL_REQUIRED_DEFAULT 20  // required for TBU conformance test
#endif
#endif

#endif /* R_BR_AUTHENTICATOR_ENABLED */


#endif /* R_AUTH_CONFIG_H */
