/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

#ifndef R_AUTH_CERTS_CONFIG
#define R_AUTH_CERTS_CONFIG

#include <stdint.h>
#include "r_nwk_inter_config.h"

#if !R_DEV_DISABLE_AUTH

#ifndef R_AUTH_CERTS_CAPACITY
#if defined(__ICCRL78__)
#define R_AUTH_CERTS_CAPACITY 3500
#else
#define R_AUTH_CERTS_CAPACITY 4086
#endif /* defined(__ICCRL78__) */
#endif

#if R_AUTH_RAM_CERTS
extern unsigned char r_auth_certs[R_AUTH_CERTS_CAPACITY];
#else
extern const unsigned char r_auth_certs[];
#endif

#if R_AUTH_MULTIPLE_CERTS
extern unsigned char r_auth_certs_use_alternate;
#if R_AUTH_RAM_CERTS
extern unsigned char r_auth_certs_alternate[R_AUTH_CERTS_CAPACITY];
#else
extern const unsigned char r_auth_certs_alternate[];
#endif
#endif

/**
 * Return TRUE if ptr points to a memory location within r_auth_certs.
 * @details This is used by the patched mbed TLS code to skip copying the node's own certificates to the heap.
 * @param ptr pointer to the beginning of a certificate.
 * @return 1 if ptr points to a memory location within r_auth_certs.
 */
int R_APP_Certs_Contains(const unsigned char* ptr);

/**
 * User callback function for custom validation of the Subject Alternative Name extension (SANE) within certificates
 * encountered during authentication.
 * @details This function is called for both our own certificate and received certificates from other devices.
 * @param subject_alt_names Buffer containing the ASN1 encoded Subject Alternative Name extension.
 * @param names_length The size of the input buffer in bytes.
 * @param ownCert True, if the passed certificate is our own. False, if the certificate was sent by another device.
 * @return 0 if the SANE is considered valid. Any other value if the SANE is considered invalid.
 */
int R_APP_Certs_ValidateSubjectAltNameExtension(const uint8_t* subject_alt_names, uint16_t names_length, int ownCert);

#endif /* !R_DEV_DISABLE_AUTH */

#endif /* R_AUTH_CERTS_CONFIG */
