/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      wpa-wrapper.h
   \version   1.00
   \brief     Wrapper for OS/standard lib functions required by WPA and mbedTLS
 */


#include <string.h>

#include "r_nwk_api.h"

#include "includes.h"
#include "common.h"
#include "utils/os.h"

// LCOV_EXCL_START

// guarded so we can also compile on unix where the functions are present
#ifdef OS_NO_C_LIB_DEFINES

// the alloc functions are in a separate file to allow easily switching dynamic memory allocators

// from os.h with OS_NO_C_LIB_DEFINES
void* os_memcpy(void* dest, const void* src, size_t n)
{
    return memcpy(dest, src, n);
}

void* os_memmove(void* dest, const void* src, size_t n)
{
    return memmove(dest, src, n);
}

void* os_memset(void* s, int c, size_t n)
{
    return memset(s, c, n);
}

int os_memcmp(const void* s1, const void* s2, size_t n)
{
    return memcmp(s1, s2, n);
}

char* os_strdup(const char* s)
{
    size_t size = os_strlen(s) + 1;
    char* dup = os_malloc(size);
    return dup ? os_memcpy(dup, s, size) : NULL;
}

size_t os_strlen(const char* s)
{
    return strlen(s);
}

#if __unix__
// only used in wpa_debug.c
int os_strcasecmp(const char* s1, const char* s2)
{
    int strcasecmp(const char*, const char*);
    return strcasecmp(s1, s2);
}
// unused
// int os_strncasecmp(const char *s1, const char *s2, size_t n);
#endif

char* os_strchr(const char* s, int c)
{
    return strchr(s, c);
}

char* os_strrchr(const char* s, int c)
{
    return strrchr(s, c);
}

int os_strcmp(const char* s1, const char* s2)
{
    return strcmp(s1, s2);
}

int os_strncmp(const char* s1, const char* s2, size_t n)
{
    return strncmp(s1, s2, n);
}

char* os_strstr(const char* haystack, const char* needle)
{
    return strstr(haystack, needle);
}

int os_snprintf(char* str, size_t size, const char* format, ...)
{
    return 0;
}
#endif /* OS_NO_C_LIB_DEFINES */

// from os.h
int os_get_time(struct os_time* t)
{
    memset(t, 0, sizeof(*t));
    return -1;
}

int R_AUTH_InternalRandom(void* unused, unsigned char* output, size_t output_len);
int os_get_random(unsigned char* buf, size_t len)
{
    return R_AUTH_InternalRandom(NULL, buf, len) ? -1 : 0;
}

int os_get_reltime(struct os_reltime* t)
{
    memset(t, 0, sizeof(*t));
    return -1;
}


// copied from os_internal.c
int os_memcmp_const(const void* a, const void* b, size_t len)
{
    const u8* aa = a;
    const u8* bb = b;
    size_t i;
    u8 res;

    for (res = 0, i = 0; i < len; i++)
    {
        res |= aa[i] ^ bb[i];
    }

    return res;
}

// copied from os_unix.c
void* os_memdup(const void* src, size_t len)
{
    void* r = os_malloc(len);

    if (r)
    {
        os_memcpy(r, src, len);
    }
    return r;
}

// LCOV_EXCL_STOP
