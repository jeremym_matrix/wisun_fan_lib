# Test certificates for the Renesas Wi-SUN FAN stack

## Description
This directory contains various X509 certificates that may be used for Wi-SUN testing and development tasks.

The details of a certificate (assuming that the filename is "testcert.x509") can be viewed in a terminal with:
```shell
openssl x509 -inform der -in "testcert.x509" -text
```

The `certificates` directory contains two certificate chains that may be used for the Wi-SUN FAN stack:
1. "Wi-SUN Test" certificates that have been created using the official Wi-SUN FAN certificate creation tool.
2. IDevIDs (IEEE Std 802.1AR-2018) issued by GlobalSign

### Wi-SUN Test Certificates

The `WisunTest/demo` directory contains various certificates that have been created using version 17 of the official Wi-SUN FAN certificate creation tool.

* root: the Wi-SUN FAN root certificate embedded in the certificate creation tool
* MCA: a Manufacturer CA signed by the Wi-SUN FAN root CA
* MICA: a Manufacturer Intermediate CA signed by the MCA
* DevFromRoot-X: a device certificate signed by Wi-SUN FAN root CA
* DevFromMica-X: a device certificate signed by the MCA

The `DevFromX-1` certificates are for the border router: they contain the 'TLS Web Server Authentication' Extended Key Usage attribute.

The other `DevFromX-Y` certificates are for router nodes: they contain the 'TLS Web Client Authentication' Extended Key Usage attribute.

The `x509` files are the certificates. The `pkcs8` files are the private keys.

The certificate hierarchy:

    root
    ├─ DevFromRoot-X
    └─ MCA
       └─ MICA
          └─ DevFromMica-X


### GlobalSign Certificates
In addition to the _Wi-SUN Test_ certificates, we also provide IDevIDs issued by GlobalSign. These certificates may be used in the way as the Wi-SUN Test certificates. 

Currently, the GlobalSign certificates are only available for router nodes (clients). Therefore, the default setting in our testing environment is to use GlobalSign IDevIDs for the client(s) (router nodes) and a Wi-SUN Test certificate for the server (border router).


## Encoding
The `encode` directory contains a tool to encode the certificates for use in the firmware. The shell scripts within the `examples` directory demonstrate how to use this tool to convert test certificates for the usage within the firmware. For each example, three versions of the encoded certificates are created:
1. A hybrid version that may be used for server OR client devices. It contains all required CA certificates for server and client, the server certificate and the client certificate(s). These files are suitable for a firmware that supports both, border router or router node configuration, where the device type is determined at runtime.
2. A server version that may only be used for the authentication server (border router). It contains all trusted CA certificates for the authentication server and and the server certificate. These files are suitable for a border router firmware. It saves memory by avoiding the overhead of the client certificate compared to the hybrid version. These files are suffixed with `_server_only`.
3. A client version that may only be used for supplicants (router nodes). It contains the trusted root CA certificates and the client certificate(s). These files are suitable for a route node firmware. It saves memory by avoiding the overhead of the trusted CA certificates and the server certificate compared to the hybrid version. These files are suffixed with `_client_only`.

For detailed usage instructions of the encoding script, please refer to the [encode/Readme.md](encode/Readme.md) file.
