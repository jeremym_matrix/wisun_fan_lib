# Encoding of certificates for the Renesas Wi-SUN FAN stack

## Overview
Note as of FANTPS v1.28 (Dec 2018), intermediate certificates of the supplicant _must_ be provisioned on the server. The reverse is not specified (neither required nor forbidden) so we assume that the server must send its intermediate certificates to the client during the EAP-TLS handshake. See the Wi-SUN FAN specification for the suggested certificate hierarchies.

We use the following the approach:
* The trusted root CA certificate(s) must be provisioned on both, the BR and supplicants
* Intermediate certificates necessary to validate the counterpart are only required on the BR
* The certificate chains may differ and it is not necessary to provide the chain to validate one's own certificate
* Additionally, the device certificate and corresponding private key are provisioned on each device (including the BR)
* For the BR, it is allowed to include the certificate chain up to but not including the trusted root CA
    * It depends on the installed trusted intermediate CAs of the device in the network if this is necessary

The Renesas Wi-SUN FAN stack expects the certificates as a blob with the encoding described below. This provides significant flexibility and allows to easily change the certificates at runtime as a configuration step.

## Encoding Tool
The encoding is performed by the Ruby script `encode-certs`, which outputs either a header file or a binary file to standard output (stdout). Execute the script without parameters for detailed usage instructions.

The following example shows the encoding of a single CA, the _DevFromRoot-1_ key and certificate for the server, and the _DevFromMICA-2_ key and certificate followed by its certification chain for the client. Note that this example targets a firmware where the role of the node (authenticator vs. supplicant) is determined at runtime since it contains both server and client certificates.

```shell
encode-certs root.x509 -- DevFromRoot-1.pkcs8 DevFromRoot-1.x509 -- \
    DevFromMICA-2.pkcs8 DevFromMICA-2.x509 MICAFromMCA-3.x509 MCA-2.x509 \
    > encoded_certs.h
```

If the device is known at compile-time, unnecessary certificates may be omitted for the encoding to save memory on the device. 

The following command encodes only the server information for the example above:
```shell
encode-certs root.x509 -- DevFromRoot-1.pkcs8 DevFromRoot-1.x509 -- \
    > encoded_certs.h
```

In contrast, this command encodes only the client information:
```shell
encode-certs root.x509 -- -- \
    DevFromMICA-2.pkcs8 DevFromMICA-2.x509 MICAFromMCA-3.x509 MCA-2.x509 \
    > encoded_certs.h
```

## Encoding Details
Each certificate/private key is prefixed with a 3 byte header. The certificate/private key itself must be provided in DER format.
- 1 Byte: index
    - 0: trusted CA
    - 1: border router/authenticator/server
    - n(>1): router node/supplicant/client
- 2 Byte little endian: size of the certificate or key
- variable: the bytes of the certificate or key (DER format)

All certificates and keys must be concatenated in a single array. While the trusted CA certificates are required on all nodes, having multiple other certificates targets primarily testing environments with a single firmware where the role (authenticator/server vs. supplicant/client) is only determined at runtime.

Note: the same index value can appear multiple times. On the one hand, it is possible to add multiple trusted CAs. On the other hand, for the server and client certificate multiple entries are required that preserve the following order:
- private key
- own certificate
- [intermediate certificates up to but not including root]

Furthermore, it is possible to add the chain of signing certificates bottom up starting with the parent certificate up to - but not including - the trusted CA certificate (if the own certificate is signed by a CA). Note: as of FANTPS v1.28, this is not allowed for supplicant certificates anymore (see above).

