/**
 *
 *  Copyright (c) 2012-2017 - Silver Spring Networks Inc.
 *
 * This program is provided for the use of the Wi-SUN Alliance
 * as a mechanism to generate example Wi-SUN certificates, and may NOT
 * be used for any other commercial purpose.  SSN and the author
 * provide no warranties or guarantees for the correctness of this
 * code - USER ASSUMES ALL RISK.
 *
 * Users may make changes and recompile as necessary to facilitate
 * their evaluation of Wi-SUN certificates.
 *
 * This program depends on libraries from the Bouncy Castle -
 * www.bouncycastle.org.
 *
 * See readme file for compile and run instructions.
 *
 * Edit History:
 * v12.0 Jun 4 2012 [Paul Duffy] Conversion to BouncyCastle is now working, change the MCA path length constraint to 1, version number support added, version removed from file name.
 * v13.0 Nov 7 2012 [Gordon Lum] (1) Use real CSEP enterprise number: 40732.   (2) Use user-entered hwType device OID in getHardwareModuleSubjAltName().
 * v14.0 Feb 14 2013 [Gordon Lum][Mike St.Johns] (1) Certificate Valid:notAfter set to '99991231235959Z'.   (2) Use Type 2 subjectKeyIdentifier.
 * v15.0 Mar 11 2013 [Gordon Lum] (1) Root, MCA, MICA: set basicConstraints: Critical=TRUE
 *                                (2) Root CA: add keyUsage: crlSign
 *                                (3) Device: add keyUsage: CRITICAL, digitalSignature, keyAgreement
 *                                (4) new sep2.keystore with updated Root CA Certificate
 * v16.0 Jun 28 2017 [Paul Duffy]
 * (1) Converted any SEP2/CSEP naming to WiSUN
 * (2) Converted to WiSUN OID
 * (3) Dedicated folder for cert and private key outputs.
 * (4) Usability improvements in the command line.
 * (5) Fixed id-on-hardwareModuleName to correct value of 1.3.6.1.5.5.7.8.4
 * (6) Removed certificatePolicies from device certificates.
 * (7) Added extendedKeyUsage extension to device certificates.
 *
 * v17.0 Aug 4 2017 [Paul Duffy]
 * (1) Added device certificate options for Client and Server Authentication EKU.
 * 
 * v18.0 Oct 16 2018 [Paul Duffy]
 * (1) Added command option 7 which accepts an ascii serial number from the console, then generates a GlobalSign Postman certificate request 
 * body (to be pasted into the Postman certificate request body), the private key file, and an empty file to receive the result of the certificate 
 * request (pasted from the certificate retrieval response).
 * (2) Added command option 8 to format/correct the Postman certificate retrieval response into correct PEM format.  
 */ 

import static org.bouncycastle.asn1.x509.KeyUsage.cRLSign;
import static org.bouncycastle.asn1.x509.KeyUsage.digitalSignature;
import static org.bouncycastle.asn1.x509.KeyUsage.keyCertSign;
import static org.bouncycastle.asn1.x509.KeyUsage.keyAgreement;
import static org.bouncycastle.asn1.x509.X509Extension.basicConstraints;
import static org.bouncycastle.asn1.x509.X509Extension.authorityKeyIdentifier;
import static org.bouncycastle.asn1.x509.X509Extension.certificatePolicies;
import static org.bouncycastle.asn1.x509.X509Extension.keyUsage;
import static org.bouncycastle.asn1.x509.X509Extension.extendedKeyUsage;
import static org.bouncycastle.asn1.x509.X509Extension.subjectAlternativeName;
import static org.bouncycastle.asn1.x509.X509Extension.subjectKeyIdentifier;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.Console;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.StringWriter;
import java.math.BigInteger;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.ECGenParameterSpec;

import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.TimeZone;
import java.util.Arrays;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;

public class GenWisunTestCerts {
	/*
	 * INCREMENT THIS VALUE WHEN A NEW VERSION IS RELEASED,
	 * AND INCLUDE A CLEAR NOTE IN THE EDIT HISTORY ABOVE.
	 */
	public static final String version = "18.0";

	public enum Devtag {
		hex, ascii, decimal
	};

	// Setup device policy - just the generic device type.

	public static final String wisunRoot = "1.3.6.1.4.1.45605";
	public static final String fieldAreaNetwork = wisunRoot + ".1";
	public static final String fanGenericDevice = fieldAreaNetwork + ".1";

	public static final ASN1ObjectIdentifier idFan = new ASN1ObjectIdentifier(
			fieldAreaNetwork);

	public static final ASN1ObjectIdentifier idFanGenericDevice = new ASN1ObjectIdentifier(
			fanGenericDevice);
	public static final PolicyInformation infFanGenericDevice = new PolicyInformation(
			idFanGenericDevice);

	public static final DERSequence mcaPolicies = new DERSequence(infFanGenericDevice);
	public static final DERSequence micaPolicies = mcaPolicies;
	public static final DERSequence devPolicies = mcaPolicies;

	// And anyPolicy - not currently used, but could be used in MCA
	public static final ASN1ObjectIdentifier anyPolicy = new ASN1ObjectIdentifier(
			"2.5.29.32.0");

	public static final PolicyInformation infAnyPolicy = new PolicyInformation(
			anyPolicy);

	public static final DERSequence anyPolicies = new DERSequence(infAnyPolicy);

	// Set up the stuff for the SubjectAltName:otherName:hardwareModuleName

	public static final String hardwareModuleName = "1.3.6.1.5.5.7.8.4";

	public static final DERObjectIdentifier idOnHardwareModuleName = new DERObjectIdentifier(
			hardwareModuleName);

	// And various key usage blobs.
	public static final KeyUsage usageCertSign = new KeyUsage(keyCertSign);
	public static final KeyUsage usageCertSignCrl = new KeyUsage(keyCertSign
			| cRLSign);
	public static final KeyUsage usageDigSigKeyAgree = new KeyUsage(digitalSignature | keyAgreement);

	// All the certs are EC P256 certs

	public static final ECGenParameterSpec ecspec = new ECGenParameterSpec(
			"secp256r1");

	static final char[] pass = "password".toCharArray();
	static final KeyStore.PasswordProtection passprot = new KeyStore.PasswordProtection(
			pass);

	static final Console con = System.console();
	static final String menuString = "GenWisunTestCerts Version " + version + "\n"
			+ "0: Save preferences and the key store and exit.\n"
			+ "1: Update Preferences\n"
			+ "----------------\n"
			+ "2: Generate an MCA\n"
			+ "3: Generate an MCA-signed MICA\n"
			+ "4: Generate a Root-signed MICA\n"
			+ "5: Generate a MICA-signed device\n"
			+ "6: Generate a Root-Signed device\n"
			+ "7: Generate a Globalsign/Postman test certificate request body and private key file\n"
			+ "8: Fix line terminations in Globalsign certificate retreival PEM response\n"
			+ "----------------\n"
			+ "10: Dump a certificate and key to a file\n"
			+ "11: Display a certificate\n"
			+ "12: Erase all certs (except the root) from the key store\n\n";

	public static void writeCertAndKey(String tag, KeyStore.PrivateKeyEntry item)
			throws Exception {
		FileOutputStream fos = new FileOutputStream("certs\\" + tag + ".x509");
		fos.write(item.getCertificate().getEncoded());
		fos.close();
		fos = new FileOutputStream("certs\\" + tag + ".pkcs8");
		fos.write(item.getPrivateKey().getEncoded());
		fos.close();
	}
	
	@SuppressWarnings("unused")
	public static void writeReqBodyAndPrivateKey(String tag, String publicKeyPEM, PrivateKey privateKey, String idOnHardwareModuleNameStr)
			throws Exception {	
		//
		// Generate the POST request.
		//
		String fmt = String.format("{ \"san\":{ \"other_names\":[ { \"type\":\"1.3.6.1.5.5.7.8.4\", \"value\": \"%s\" } ] }, \"public_key\": \"%s\", \"validity\": { \"not_before\": {{not_before}}, \"not_after\": {{not_after}} } }", idOnHardwareModuleNameStr, publicKeyPEM);				
		FileOutputStream fos = new FileOutputStream("certs\\postmanReqBody-" + tag + ".json");
		fos.write(fmt.getBytes());
		fos.close();
		//
		// Generate the private key file.
		//
		fos = new FileOutputStream("certs\\privateKey-" + tag + ".pkcs8");
		fos.write(privateKey.getEncoded());
		fos.close();
		//
		// Convenience ... generate an empty file to receive the result of the GS test certificate request.
		//
		fos = new FileOutputStream("certs\\idevid-" + tag + ".txt");
		fos.close();		
	}	

	/**
	 * Perform initial setup for a certificate builder.
	 * Additional extensions are added and the generation occurs later.
	 */
	public static X509v3CertificateBuilder genCertBuilder(String subject,
			KeyStore.PrivateKeyEntry signer, int ca, int expire, KeyPair kp)
			throws Exception {

		/*
		 * Calculate various values to be used by the certificate builder constructor.
		 */

		Calendar now = Calendar.getInstance();
		now.add(Calendar.DAY_OF_MONTH, -1);
		Date notBefore = now.getTime();
		now.add(Calendar.MONTH, expire);
//		Date notAfter = now.getTime();

		// Set up a date as 31 Dec 9999, 23:59:59
		Calendar c = Calendar.getInstance();
		c.clear();
		c.setTimeZone(TimeZone.getTimeZone("GMT-0"));
		c.set(Calendar.ZONE_OFFSET, 0);
		c.set(Calendar.YEAR, 9999);
		c.set(Calendar.MONTH, 11); // this is actually december
		c.set(Calendar.DATE, 31);
		c.set(Calendar.HOUR, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		Date notAfter = c.getTime();

		X500Name subjectDN = null;
		if (subject != null)
			subjectDN = new X500Name(subject);

		X500Name issuerDN = null;
		if (signer == null) {
			issuerDN = new X500Name(subject);
		} else {
//			issuerDN = X500Name.getInstance(ASN1Primitive.fromByteArray(((X509Certificate)signer.getCertificate()).getIssuerX500Principal().getEncoded()));	// Michael's suggestion.
			issuerDN = new X500Name(((X509Certificate) signer.getCertificate()).getSubjectDN().getName());
		}

		System.out.printf("Building certificate with...\n");
		System.out.printf("subjectDN: %s \n", subjectDN);
		System.out.printf("issuerDN: %s \n", issuerDN);

		SubjectPublicKeyInfo spki = new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(kp.getPublic().getEncoded())).readObject());
		BigInteger serialNumber = new BigInteger(32, new SecureRandom());
		/*
		 * Construct the cert builder.
		 */
		X509v3CertificateBuilder cg = new X509v3CertificateBuilder(issuerDN,
				serialNumber, notBefore, notAfter, subjectDN, spki);
		/*
		 * Add a few extensions.
		 *
		 * CA >= 0 is a ca with this path length. CA = -1 is CA with no limits,
		 * -2 is end entity with with subjectKeyId, CA = -3 is end entity
		 * without subjectKeyId
		 */

		if (ca >= 0) {
			cg.addExtension(basicConstraints, true, new BasicConstraints(ca));
		}
		if (ca == -1) {
			cg.addExtension(basicConstraints, true, new BasicConstraints(true));
		}

		if (ca >= -2) {
//			cg.addExtension(subjectKeyIdentifier, false,
//					new SubjectKeyIdentifierStructure(kp.getPublic()));

			/*
			* Create Type 2 Key Identifier.  RFC 5280 defines this type as:
			*
			*  (2) The keyIdentifier is composed of a four-bit type field with
			*      the value 0100 followed by the least significant 60 bits of
			*      the SHA-1 hash of the value of the BIT STRING
			*      subjectPublicKey (excluding the tag, length, and number of
			*      unused bits).
			*
			* Could not find a working method that directly computes a Type2 KeyId.
			* So, manually create it by starting with a Type1 KeyId.
			*/

			// Generate 20-Byte Type1 Key Identifier (SHA-1 hash of public key).
			byte Type1KeyId[] = new SubjectKeyIdentifierStructure(kp.getPublic()).getKeyIdentifier();
			// Copy the last 8 bytes of the key identifier
			byte Type2KeyId[] = Arrays.copyOfRange(Type1KeyId, 12, 20);
			// tag the first byte for type 2.
			Type2KeyId[0] = (byte)((Type2KeyId[0] & 0x0f) | 0x40);

			cg.addExtension(subjectKeyIdentifier, false, new SubjectKeyIdentifier(Type2KeyId));
		}


		if (signer != null) {
			byte[] extValue = ((X509Certificate) signer.getCertificate())
					.getExtensionValue("2.5.29.14");

			SubjectKeyIdentifierStructure signerId = new SubjectKeyIdentifierStructure(
					extValue);

			cg.addExtension(authorityKeyIdentifier, false,
					new AuthorityKeyIdentifier(signerId.getKeyIdentifier()));
		}

		return cg;
	}

	/**
	 * Generate the keystore private key entry from a certificate builder.
	 */
	public static KeyStore.PrivateKeyEntry genCert(X509v3CertificateBuilder cb,
			KeyStore.PrivateKeyEntry signer,
			KeyPair kp) throws Exception {

		/*
		 * Determine the signing key.
		 */
		PrivateKey signKey;
		if (signer == null) {
			signKey = kp.getPrivate();
		} else {
			signKey = signer.getPrivateKey();
		}

		/*
		 * Sign the certificate.
		 */
		ContentSigner sigGen = new JcaContentSignerBuilder(getSigalgByKey(signKey)).setProvider("BC").build(signKey);
		X509CertificateHolder ch = cb.build(sigGen);
		Certificate[] certs = new Certificate[1];
		certs[0] = new JcaX509CertificateConverter().setProvider( "BC" )
		  .getCertificate(ch);
		/*
		 * Store the signed cert in the keystore.
		 * NOTE: the cert is signed with signKey, but stored with kp.Private ... problem?
		 */
		return new KeyStore.PrivateKeyEntry(kp.getPrivate(), certs);
	}

	public static GeneralNames getHardwareModuleSubjAltName(byte[] mac,
			DERObjectIdentifier devType) {
		return new GeneralNames(new GeneralName(GeneralName.otherName,
				new DERSequence(new ASN1Encodable[] {
						idOnHardwareModuleName,
						new DERTaggedObject(true, 0, new DERSequence(
								new ASN1Encodable[] { devType,
										new DEROctetString(mac) })) })));

	}

	public static String getSigalgByKey(PrivateKey key) {
		int bitlength;

		if (key instanceof RSAPrivateKey) {
			bitlength = ((RSAPrivateKey) key).getPrivateExponent().bitLength();
			if (bitlength > 2048)
				return "SHA384withRSA";
			if (bitlength > 1536)
				return "SHA256withRSA";
			return "SHA1withRSA";

		} else if (key instanceof ECPrivateKey) {
			bitlength = ((ECPrivateKey) key).getParams().getCurve().getField()
					.getFieldSize();

			if (bitlength > 256)
				return "SHA384withECDSA";
			if (bitlength > 224)
				return "SHA256withECDSA";
			return "SHA224withECDSA";

		}
		return "Unknown";

	}

	public static void main(String[] args) throws Exception {

		Security.insertProviderAt(new BouncyCastleProvider(), 1);
		FileInputStream keystreamin = null;
		FileOutputStream keystreamout = null;
		KeyStore.PrivateKeyEntry root = null;

		Preferences prefs = Preferences.userRoot().node("wisun.certs");
		KeyStore ks = KeyStore.getInstance("jks");
		try {
			keystreamin = new FileInputStream("wisun.keystore");
			ks.load(keystreamin, pass);
			keystreamin.close();

			root = (KeyStore.PrivateKeyEntry) ks.getEntry("root", passprot);

		} catch (FileNotFoundException ex) {
			System.out.println("Key store not found");

		}

		KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC", "BC");
		kpg.initialize(ecspec);

		if (root == null) {
			if (args.length > 0) {
				//hidden option to generate a new keystore and root.
				//
				KeyPair kp = kpg.generateKeyPair();

				X509v3CertificateBuilder cb = genCertBuilder(
						"O=WiSUN, CN=WiSUN Root, serialNumber=1", null, -1, 360,
						kp);
				cb.addExtension(keyUsage, true, usageCertSignCrl);
				cb.addExtension(certificatePolicies, true, anyPolicies);

				root = genCert(cb, null, kp);
				try {
					keystreamout = new FileOutputStream("wisun.keystore");
					ks.load(null, pass);
					ks.setEntry("root", root, passprot);
					ks.store(keystreamout, pass);
					keystreamout.close();
				} catch (Exception ex) {
					System.out.println("Unable to write keystore with root:"
							+ ex);
					System.exit(1);
				}
			} else {
				System.out
						.println("Can't find the root or the root key store - file is 'wisun.keystore'");
				System.exit(1);
			}
		}

		// Initialize the preferences stuff.
		//
		int certSerialNumber = prefs.getInt("serial", 2);
		prefs.putInt("serial", certSerialNumber);
		doUpdatePrefs(prefs, false);

		String deviceOid = prefs.get("deviceOid", "");
		String companyName = prefs.get("companyName", "");
		String devNameType = prefs.get("devnameType", "");

		Devtag dt = Devtag.hex;

		if (devNameType.equalsIgnoreCase("d")) {
			dt = Devtag.decimal;
		} else if (devNameType.equalsIgnoreCase("a")) {
			dt = Devtag.ascii;
		}

		System.out.printf(
				"%n%nPreferences...%nCompany Name: '%s'%nDevice OID: '%s'%nInput type: %s%n%n",
				companyName, deviceOid, dt);

		DERObjectIdentifier devObjectId = new DERObjectIdentifier(deviceOid);

		// System.exit(1);

		int cmd = 0;
		String tagname = "";
		byte[] bytename = null;
		String clientOrServer = "";

		Vector<ASN1Object> deviceEKU = new Vector<ASN1Object>();

		cmdloop: do {
//			System.out.printf("\nCertificate Serial Number: %d%n", certSerialNumber);
			System.out.println("\n" + menuString);
			try {
				cmd = Integer.parseInt(con.readLine("Enter option: "));
			} catch (NumberFormatException ex) {
				continue;
			}

			KeyStore.PrivateKeyEntry certAndKey = null;
			KeyStore.PrivateKeyEntry signer = null;
			String alias = null;
			KeyPair kp = null;
			X509v3CertificateBuilder cb = null;

			switch (cmd) {

			case 1: // update preferences
				doUpdatePrefs(prefs, true);

				devObjectId = new DERObjectIdentifier(
						prefs.get("deviceOid", ""));
				companyName = prefs.get("companyName", "");
				devNameType = prefs.get("devnameType", "");

				dt = Devtag.hex;

				if (devNameType.equalsIgnoreCase("d")) {
					dt = Devtag.decimal;
				} else if (devNameType.equalsIgnoreCase("a")) {
					dt = Devtag.ascii;
				}

				break;
			case 2: // Gen an MCA

				kp = kpg.generateKeyPair();

				cb = genCertBuilder(String.format(
						"C=US, O=%s, CN=WiSUN MCA, serialNumber=%d", companyName,
						certSerialNumber), root, 1, 360, kp);
				cb.addExtension(keyUsage, true, usageCertSign);
				cb.addExtension(certificatePolicies, true, mcaPolicies);
				certAndKey = genCert(cb, root, kp);

				alias = String.format("mca-%d", certSerialNumber);
				System.out.println("Gen'd the cert");
				ks.setEntry(alias, certAndKey, passprot);
				System.out.printf(
						"Writing cert and key to 'MCA-%d.[x509,pkcs8]'%n%n",
						certSerialNumber);
				writeCertAndKey(String.format("MCA-%d", certSerialNumber++), certAndKey);
				break;
			case 3: // Gen the MICA from the MCA

				signer = findCert(ks, "mca", "Select MCA to sign the MICA: ");
				if (signer == null) {
					System.out
							.println("Can't find an MCA with which to sign; generate one first.");
					break;
				}

				kp = kpg.generateKeyPair();

				cb = genCertBuilder(String.format(
						"C=US, O=%s, CN=WiSUN MICA, serialNumber=%d",
						companyName, certSerialNumber), signer, 0, 360, kp);
				cb.addExtension(keyUsage, true, usageCertSign);
				cb.addExtension(certificatePolicies, true, micaPolicies);
				certAndKey = genCert(cb, signer, kp);

				alias = String.format("mica-%d", certSerialNumber);
				ks.setEntry(alias, certAndKey, passprot);
				System.out
						.printf("Writing cert and key to 'MICAFromMCA-%d.[x509,pkcs8]'%n",
								certSerialNumber);
				writeCertAndKey(String.format("MICAFromMCA-%d", certSerialNumber++),
						certAndKey);
				break;
			case 4: // Gen the MICA from the root

				kp = kpg.generateKeyPair();

				cb = genCertBuilder(String.format(
						"C=US, O=%s, CN=WiSUN MICA, serialNumber=%d",
						companyName, certSerialNumber), root, 0, 360, kp);
				cb.addExtension(keyUsage, true, usageCertSign);
				cb.addExtension(certificatePolicies, true, micaPolicies);
				certAndKey = genCert(cb, root, kp);

				alias = String.format("mica-%d", certSerialNumber);
				ks.setEntry(alias, certAndKey, passprot);
				System.out
						.printf("Writing cert and key to 'MICAFromRoot-%d.[x509,pkcs8]'%n",
								certSerialNumber);
				writeCertAndKey(String.format("MICAFromRoot-%d", certSerialNumber++),
						certAndKey);
				break;
			case 5: // Gen a device certificate from a MICA

				signer = findCert(ks, "mica",
						"Select a MICA to sign the device cert: ");
				if (signer == null) {
					System.out
							.println("Can't find an MICA with which to sign; generate one first.");
					break;
				}

				do {
					tagname = con.readLine("Enter a %s device serial number: ",
							dt);
					if (tagname.length() == 0)
						continue;
					bytename = getBytesForName(tagname, dt);
					if (bytename != null)
						break;

				} while (true);

				deviceEKU.clear();
				deviceEKU.add(idFan);

				do {
					clientOrServer = con.readLine("Client [c] or Server [s] certificate? : ",
							dt);
					if (clientOrServer.length() == 0)
						continue;
					if (clientOrServer.equalsIgnoreCase("c"))
					{
						deviceEKU.add(KeyPurposeId.id_kp_clientAuth);
						break;
					}
					else if (clientOrServer.equalsIgnoreCase("s"))
					{
						deviceEKU.add(KeyPurposeId.id_kp_serverAuth);
						break;
					}
				} while (true);

				kp = kpg.generateKeyPair();

				cb = genCertBuilder(null, signer, -3, 360, kp);
				cb.addExtension(keyUsage, true, usageDigSigKeyAgree);
				cb.addExtension(subjectAlternativeName, true,
						getHardwareModuleSubjAltName(bytename, devObjectId));
//				cb.addExtension(certificatePolicies, true, devPolicies);
//  			cb.addExtension(extendedKeyUsage, true, new ExtendedKeyUsage(new DERSequence(idFan)));
				cb.addExtension(extendedKeyUsage, true, new ExtendedKeyUsage(deviceEKU));
				certAndKey = genCert(cb, signer, kp);

				System.out
						.printf("Writing cert and key to 'DevFromMICA-%s.[x509,pkcs8]'%n",
								tagname);
				writeCertAndKey(String.format("DevFromMICA-%s", tagname),
						certAndKey);
				break;
			case 6: // Gen a device cert from a root

				do {
					tagname = con.readLine("Enter a %s device serial number: ",
							dt);
					if (tagname.length() == 0)
						continue;
					bytename = getBytesForName(tagname, dt);
					if (bytename != null)
						break;
				} while (true);

				deviceEKU.clear();
				deviceEKU.add(idFan);

				do {
					clientOrServer = con.readLine("Client [c] or Server [s] certificate? : ",
							dt);
					if (clientOrServer.length() == 0)
						continue;
					if (clientOrServer.equalsIgnoreCase("c"))
					{
						deviceEKU.add(KeyPurposeId.id_kp_clientAuth);
						break;
					}
					else if (clientOrServer.equalsIgnoreCase("s"))
					{
						deviceEKU.add(KeyPurposeId.id_kp_serverAuth);
						break;
					}
				} while (true);

				kp = kpg.generateKeyPair();

				cb = genCertBuilder(null, root, -3, 360, kp);
				cb.addExtension(keyUsage, true, usageDigSigKeyAgree);
				cb.addExtension(subjectAlternativeName, true,
						getHardwareModuleSubjAltName(bytename, devObjectId));
//				cb.addExtension(certificatePolicies, true, devPolicies);
//  			cb.addExtension(extendedKeyUsage, true, new ExtendedKeyUsage(new DERSequence(idFan)));
				cb.addExtension(extendedKeyUsage, true, new ExtendedKeyUsage(deviceEKU));
				certAndKey = genCert(cb, root, kp);

				System.out
						.printf("Writing cert and key to 'DevFromRoot-%s.[x509,pkcs8]'%n",
								tagname);
				writeCertAndKey(String.format("DevFromRoot-%s", tagname),
						certAndKey);
				break;

			case 7:
				// Generate 3 artifacts for a device.
				//
				// 1. A Globalsign Postman request body for test entity (IDevID) certificate generation.
				// 2. The associated private key file for the certificate to be generated by the request.
				// 3. An empty file ready to receive the PEM response cut/pasted from the GS certificate retrieval response body.
				//
				// This is the start of what could eventually be turned into a full Globalsign RESTful API client.
				// Hopefully Globalsign provides a viable API client BEFORE Wi-SUN resources such a client.
				//
				
				// Get the serial number from the console.
				//
				do {
					tagname = con.readLine("Enter a %s device serial number: ", dt);
					if (tagname.length() == 0)
						continue;
					bytename = getBytesForName(tagname, dt);
					if (bytename != null)
						break;
				} while (true);

				// Generate the HardwareModule SANE
				//
				GeneralNames gn = getHardwareModuleSubjAltName(bytename, devObjectId);
			    byte[] bytes = gn.getEncoded();
			    StringBuilder idOnHardwareModuleNameStr = new StringBuilder();
			    for (byte b : bytes) {
			    	idOnHardwareModuleNameStr.append(String.format("%02X", b));
			    }				
				
				// Generate the X509 key pair.
				//
				kp = kpg.generateKeyPair();
				
				// Generate the public key in PEM format.
				//
				StringWriter strPubKey = new StringWriter();
				PemWriter pw = new PemWriter(strPubKey);
				pw.writeObject(new PemObject("PUBLIC KEY", kp.getPublic().getEncoded()));
				pw.flush();				

				// Write the Postman certificate request body and the private key files.
				//
				System.out.printf("Writing Postman Certificate Request Body and Private key for device serial number %s.\n", tagname);
				writeReqBodyAndPrivateKey(tagname, strPubKey.toString(), kp.getPrivate(), idOnHardwareModuleNameStr.toString());
				break;
				
			case 8:

				// Fix the line terminations in the Globalsign certificate retrieval PEM result.
				
				// Get the serial number from the console.
				//
				do {
					tagname = con.readLine("Enter a %s device serial number: ", dt);
					if (tagname.length() == 0)
						continue;
					bytename = getBytesForName(tagname, dt);
					if (bytename != null)
						break;
				} while (true);

				//
				// Fix the PEM file.
				//
				FileReader fr = new FileReader("certs\\idevid-" + tagname + ".txt");
				FileOutputStream fos = new FileOutputStream("certs\\idevid-" + tagname + ".pem");
				BufferedReader br = new BufferedReader(fr);
				String inLine, outLine;
				while((inLine = br.readLine()) != null)
				{
					outLine = inLine.replace("\\n", "\n");
					fos.write(outLine.getBytes());
				};
				
				fos.close();
				br.close();
				fr.close();
		
				break;
				
				
			case 10: // dump a cert
				certAndKey = findCert(ks, null,
						"Select a cert and key to dump to file: ");
				if (certAndKey == null)
					break;
				String fname = con
						.readLine("Specify file prefix for key and cert file: ");
				System.out.printf(
						"Writing cert and key to '%s.[x509,pkcs8]'%n", fname);
				writeCertAndKey(fname, certAndKey);
				break;
			case 11: // display a cert
				KeyStore.PrivateKeyEntry dc = findCert(ks, null,
						"Select a cert to display: ");
				if (dc != null) {
					System.out.println(dc.getCertificate());
				}
				System.out.println();
				break;
			case 12: // erase everything except the root
				cleanupKeyStore(ks);
				break;
			case 0: // exit
				prefs.putInt("serial", certSerialNumber);
				try {
					prefs.flush();
				} catch (BackingStoreException ex) {
					System.out.println("Unable to store preferences");
				}
				try {
					keystreamout = new FileOutputStream("wisun.keystore");
					ks.store(keystreamout, pass);
					keystreamout.close();
				} catch (Exception ex) {
					System.out.println("Error saving key store file: " + ex);
				}

				break cmdloop;
			}
		} while (true);
	}

	static void doUpdatePrefs(Preferences prefs, boolean forceupdate) {
		// if forceupdate is false, then update the prefs if and only if they
		// are null
		//
		String companyName = prefs.get("companyName", "");
		if ("".equals(companyName) || forceupdate) {

			do {
				String temp = con.readLine("Enter company name [%s]: ",
						companyName);
				companyName = "".equals(temp) ? companyName : temp;
				if (con.readLine("Use '%s' as company name? [y/n]: ",
						companyName).equalsIgnoreCase("y"))
					break;
			} while (true);
			prefs.put("companyName", companyName);

		}

		String devOid = prefs.get("deviceOid", "");
		if ("".equals(devOid) || forceupdate) {
			do {
				String temp = con.readLine("Enter device OID [%s]: ", devOid);
				devOid = "".equals(temp) ? devOid : temp;
				// check that it converts
				try {
					DERObjectIdentifier oid = new DERObjectIdentifier(devOid);
				} catch (Exception ex) {
					System.out.printf(
							"Can't convert '%s' to an OID, try again.%n%n",
							devOid);
					continue;
				}
				if (con.readLine("Use '%s' as device OID? [y/n]: ", devOid)
						.equalsIgnoreCase("y"))
					break;
			} while (true);
			prefs.put("deviceOid", devOid);
		}

		String devnameType = prefs.get("devnameType", "");
		if ("".equals(devnameType) || forceupdate) {
			do {
				String temp = con
						.readLine("Use hex, decimal or ascii for entry of device serial numbers [h, d, a]? ");
				if (temp.trim().equalsIgnoreCase("h")
						|| temp.trim().equalsIgnoreCase("d")
						|| temp.trim().equalsIgnoreCase("a")) {
					devnameType = temp;
					break;
				}
			} while (true);
			prefs.put("devnameType", devnameType);
		}
		try {
			prefs.flush();
		} catch (BackingStoreException ex) {
			System.out.println("Can't update preferences store - be careful");
		}

	}

	static void cleanupKeyStore(KeyStore ks) throws Exception {
		Enumeration<String> aliases = ks.aliases();
		String alias;
		while (aliases.hasMoreElements()) {
			alias = aliases.nextElement();
			if (alias.equalsIgnoreCase("root"))
				continue;
			ks.deleteEntry(alias);
		}
	}

	static KeyStore.PrivateKeyEntry findCert(KeyStore ks, String tag,
			String prompt) throws Exception {
		Enumeration<String> aliases = ks.aliases();
		KeyStore.PrivateKeyEntry result = null;
		KeyStore.Entry x = null;

		Vector<KeyStore.PrivateKeyEntry> v = new Vector<KeyStore.PrivateKeyEntry>();

		while (aliases.hasMoreElements()) {
			String alias = aliases.nextElement();
			if (tag != null && !alias.startsWith(tag))
				continue;
			// if (!ks.entryInstanceOf(alias, result.getClass()))
			// continue;
			x = ks.getEntry(alias, passprot);
			if (x instanceof KeyStore.PrivateKeyEntry)
				v.add((KeyStore.PrivateKeyEntry) x);

		}
		int cmd = 0;
		if (v.size() == 0)
			return null;

		System.out.println("0:  EXIT without selecting cert");
		do {
			int count = 0;
			for (KeyStore.PrivateKeyEntry e : v) {
				System.out.printf("%d:  %s%n", ++count,
						((X509Certificate) e.getCertificate()).getSubjectDN());
			}

			try {
				cmd = Integer.parseInt(con.readLine(prompt));
				if (cmd < 0 || cmd > count)
					continue;
			} catch (NumberFormatException ex) {
				continue;
			}
			if (cmd == 0)
				return null;
			else
				return v.elementAt(cmd - 1);

		} while (true);

	}

	static byte[] getBytesForName(String tag, Devtag dt) {
		switch (dt) {
		case hex:
			String temp = tag.trim();
			if (temp.length() % 2 != 0)
				temp = "0" + temp;
			byte[] res = new byte[temp.length() / 2];
			try {
				for (int i = 0; i < res.length; i++) {
					res[i] = (byte) (Integer.parseInt(
							temp.substring(i * 2, i * 2 + 2), 16) & 0xff);
				}
				return res;
			} catch (NumberFormatException ex) {
				return null;
			}
		case decimal:
			try {
				BigInteger n = new BigInteger(tag.trim());
				if (n.signum() <= 0)
					return null;
				return n.toByteArray();
			} catch (NumberFormatException ex) {
				return null;
			}
		case ascii:
			return tag.getBytes();
		}
		return null;
	}
}
