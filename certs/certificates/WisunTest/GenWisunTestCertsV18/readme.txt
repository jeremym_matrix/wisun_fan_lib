This utility provides the ability to generate test certificates (and associated private keys) for usage in Wi-SUN FAN test beds.

Device certificates can be generated directly from the Wi-SUN test root certificate, or from a Manufacturer Intermediate Certificate (which is generated from a Manufacturer Certificate which is itself generated from the Wi-SUN test root certificate).  Thus, the tool supports certificate chain depths of 2 or 4. 

The Wi-SUN test root comes pre-installed with the tool.

Generated certificates and associated private keys are placed in the \certs folder.

The tool maintains an internal serial # for the root itself and all generated MCA and MICA certificates.

For device certificates, you will be prompted for a serial number which is combined with your device OID (in your preference settings) to form the unique device identifier. 

To compile (Java required ... example I use 1.8) ...

javac -cp .;bcprov-ext-jdk15on-147.jar;bcprov-ext-jdk15on-147.jar;bcpkix-jdk15on-147.jar GenWisunTestCerts.java

To run (Java required) ...

java -cp .;bcprov-ext-jdk15on-147.jar;bcprov-ext-jdk15on-147.jar;bcpkix-jdk15on-147.jar GenWisunTestCerts

Be sure to configure your preferences for company name, device OID (later combined with a device serial number), and preferred input format.

It is recommended to start experimenting with device certificates issued from the built in Wi-SUN root, then go from there with the full chain of 4 to include an MAC amd MICA.


/*******************************************************************************
*  Version History
*******************************************************************************/
 v18.0 Oct 16 2018 [Paul Duffy]
 (1) Added command option 7 which accepts an ascii serial number from the console, then generates a GlobalSign Postman certificate request 
 body (to be pasted into the Postman certificate request body), the private key file, and an empty file to receive the result of the certificate 
 request (pasted from the certificate retrieval response).
 (2) Added command option 8 to format/correct the Postman certificate retrieval response into correct PEM format
-------------------------------------------------------------------------------- 
v17.0 Aug 4 2017 (Paul Duffy)
(1) Added device certificate options for Client and Server Authentication EKU.
--------------------------------------------------------------------------------
v16.0 Jun 28 2017 [Paul Duffy]
(1) Converted any SEP2/CSEP naming to WiSUN
(2) Converted to WiSUN OID
(3) Dedicated folder for cert and private key outputs.
(4) Usability improvements in the command line.
(5) Fixed id-on-hardwareModuleName to correct value of 1.3.6.1.5.5.7.8.4
(6) Removed certificatePolicies from device certificates.
(7) Added extendedKeyUsage extension to device certificates.
--------------------------------------------------------------------------------
v15.0 Mar 11 2013 [Gordon Lum]
(1) Root, MCA, MICA: set basicConstraints: Critical=TRUE
(2) Root CA: add keyUsage: crlSign
(3) Device: add keyUsage: CRITICAL, digitalSignature, keyAgreement
(4) new sep2.keystore with updated Root CA Certificate

--------------------------------------------------------------------------------
v14.b Feb 21 2013 [Gordon Lum]
(1) New sep2.keystore
    - Go back to using the same root keypair as version 13 (and earlier)
    - New Root CA certificate.  Changes relative to version 13 certificate:
        a) Valid:notAfter set to '99991231235959Z'
        b) Type 2 subjectKeyIdentifier is used
        c) New X.509 certificate serial number
(2) No change in the Java code.

  WARNING:
  Even though we are using the same root keypair as version 13, certificates
  generated with version 13 keystores will not chain to the new root because of
  the change to Type 2 keyIdentifiers.

--------------------------------------------------------------------------------
v14.0 Feb 14 2013 [Gordon Lum][Mike St.Johns]
(1) Certificate Valid:notAfter set to '99991231235959Z'.
(2) Use Type 2 subjectKeyIdentifier.
(3) New sep2.keystore containing new root certificate/keypair
    - New Distinguished Name (DN) = SERIALNUMBER=1, CN=CSEP Test Root, C=CSEP
    - Old Distinguished Name was  = SERIALNUMBER=1, CN=CSEP Root, C=CSEP
    - Valid:notAfter set to '99991231235959Z'
    - Type 2 subjectKeyIdentifier is used

  WARNING:
  Because of the change in the root certificate/keypair, certificates
  generated with previous versions of the keystore will no longer work as they
  chain to the old root instead of the new one.

--------------------------------------------------------------------------------
v13.0 Nov 7 2012 [Gordon Lum]
(1) Use real CSEP enterprise number: 40732.
(2) Use user-entered hwType device OID in getHardwareModuleSubjAltName().

--------------------------------------------------------------------------------
v12.0 Jun 4 2012 [Paul Duffy]
Conversion to BouncyCastle is now working, change the MCA path length constraint
to 1, version number support added, version removed from file name.
